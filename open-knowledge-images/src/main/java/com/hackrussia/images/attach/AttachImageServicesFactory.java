package com.hackrussia.images.attach;

import com.hackrussia.model.ImageCategory;

/**
 * @author Ivan
 * @since 29.08.2016
 */
public interface AttachImageServicesFactory {

    AttachImageService getAttachImageService(ImageCategory imageCategory);
}
