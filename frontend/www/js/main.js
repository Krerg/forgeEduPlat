/**
 * Created by zarza on 19.11.16.
 */

var settings = {
    remoteHost: 'http://192.168.43.31:8080',
    fetchOptions: {
        method: 'GET',
        mode: 'cors'
    },
    token: 'BEvSYVNzQC6EGSVOChUUEVST935O'
};

class progressState {
    constructor (router) {
        this.complete = {
            1: false,
            2: false,
            3: false,
            4: false,
        };
        this.router = router;
    }

    setComplete(id) {
        this.complete[id] = true;
        $('.lessons__nav_item:eq(' + (id - 1) + ')').addClass('complete');
        this.router.navigate('/lessons/stereometry/' + (id + 1));
        $('.progress_n').text(id);
        $('.js-progress').addClass('pr_' + id);
        console.log(this.getProgress());
        console.log(id)
    }

    getProgress() {
        return Object.keys(this.complete).reduce(function (acc, i) {
            return this.complete[i] ? acc + 1 : acc;
        }, 0);
    }
}


function init(urn, blockId) {
    var getToken = function () {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", settings.remoteHost + '/v1/auth/authToken', false);
//            xhr.send(null);
        return settings.token;
        // return xhr.responseText;
    };
    var options = {
        env: 'AutodeskProduction',
        getAccessToken: getToken,
        refreshToken: getToken
    };



    Autodesk.Viewing.Initializer(options, function onInitialized() {
        Autodesk.Viewing.Document.load(urn, onDocumentLoadSuccess, onDocumentLoadFailure);
    });

    /**
     * Autodesk.Viewing.Document.load() success callback.
     * Proceeds with model initialization.
     */
    function onDocumentLoadSuccess(doc) {
        // A document contains references to 3D and 2D viewables.
        var viewables = Autodesk.Viewing.Document.getSubItemsWithProperties(doc.getRootItem(), {'type': 'geometry'}, true);
        if (viewables.length === 0) {
            console.error('Document contains no viewables.');
            return;
        }

        // Choose any of the avialble viewables
        var initialViewable = viewables[0];
        var svfUrl = doc.getViewablePath(initialViewable);
        var modelOptions = {
            sharedPropertyDbPatoptionsh: doc.getPropertyDbPath()
        };

        var el = document.getElementById(blockId);

        viewer = new Autodesk.Viewing.Private.GuiViewer3D(el);
        viewer.start(svfUrl, modelOptions, onLoadModelSuccess, onLoadModelError);
    }

    /**
     * Autodesk.Viewing.Document.load() failuire callback.
     */
    function onDocumentLoadFailure(viewerErrorCode) {
        console.error('onDocumentLoadFailure() - errorCode:' + viewerErrorCode);
    }

    /**
     * viewer.loadModel() success callback.
     * Invoked after the model's SVF has been initially loaded.
     * It may trigger before any geometry has been downloaded and displayed on-screen.
     */
    function onLoadModelSuccess(model) {
        console.log('onLoadModelSuccess()!');
        console.log('Validate model loaded: ' + (viewer.model === model));
        console.log(model);
    }

    /**
     * viewer.loadModel() failure callback.
     * Invoked when there's an error fetching the SVF file.
     */
    function onLoadModelError(viewerErrorCode) {
        console.error('onLoadModelError() - errorCode:' + viewerErrorCode);
    }
}

function ready(fn) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

function login() {
    document.getElementById('contentPage').style.display = 'block';
    document.getElementById('loginPage').style.display = 'none';
}

ready(function () {
    $('.file-upload-form').submit(function (e) {
        e.preventDefault();

        var fd = new FormData();
        fd.append( "file", $("input[name=file]")[0].files[0]);
        fd.append( "referenceId", "string");

        $.ajax({
            type: "POST",
            url: settings.remoteHost + "/v1/model/model",
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            success: function (result) {
                if ( result.reseponseInfo == "SUCCESS" ) {
                    console.log(result);
                } else {
                    console.error(result);
                }
            },
            error: function (result) {
                console.log(result.responseText);
            }
        });
    });

    var $pages = $('#main, #lessons, #add_lesson, #new');
    var router = new Navigo(root = null, useHash=true);

    router
        .on(function () {
            document.getElementById('contentPage').style.display = 'none';
            document.getElementById('loginPage').style.display = 'block';
        })
        .on('/main', function () {
            login();
            $pages.fadeOut(200, function () {
                $('#main').fadeIn(200);
            });
            $('.main_menu__item').removeClass('active');
            $('.main_menu__item:eq(0)').addClass('active');
        })
        .on('/lessons/stereometry', function () {
            login();
            $pages.fadeOut(200, function () {
                $('#lessons').fadeIn(200);
            });
            $('.main_menu__item').removeClass('active');
            $('.main_menu__item:eq(1)').addClass('active');
            router.navigate('/lessons/stereometry/1');
        })
        .on('/lessons/stereometry/:id', function (params) {
            login();
            var id = params.id;
            $('.lessons__nav_item')
                .removeClass('active')
                .slice(id-1, id)
                .addClass('active');
            $('.lessons__item')
                .removeClass('active')
                .slice(id-1, id)
                .addClass('active');
        })
        .on('/new_lesson', function (params) {
            login();
            $pages.fadeOut(200, function () {
                $('#new').fadeIn(200);
            });
            $('.main_menu__item').removeClass('active');

        })
        .on('/add_lesson', function () {
            login();
            $pages.fadeOut(200, function () {
                $('#add_lesson').fadeIn(200);
            });
            $('.main_menu__item').removeClass('active');
            $('.main_menu__item:eq(2)').addClass('active');
        })
        .resolve();

    var progress = new progressState(router);

    $('.js-next-step').click(function () {
       progress.setComplete($(this).data('step-id'));
    });

    $('.js-check-answer').submit(function (e) {
        e.preventDefault();

        //TODO: Ajax check
        if (this.string && (this.string.value === "все точки прямой лежат в этой плоскости" || this.string.value === "8")) {
            progress.setComplete($(this).data('step-id'));
        } else {
            alert("Неправильный ответ");
        }
    });

    fetch(settings.remoteHost + '/rest/api/video/next', settings.fetchOptions)
        .then(function (response) {return response.json();})
        .then(function (data) {
            var $slide = $('.lessons__item:eq(0)');
            $slide.find('.lesson__title').text(data.title);
            $slide.find('.lesson__video').html(
                '<iframe id="ytplayer" type="text/html" width="640" height="360"\
                src="' + data.url + '" allowfullscreen frameborder="0"></iframe>'
            );
        });
    fetch(settings.remoteHost + '/rest/api/task/example/next', settings.fetchOptions)
        .then(function (response) {return response.json();})
        .then(function (data) {
            var $slide = $('.lessons__item:eq(1)');
            $slide.find('.lesson__title').text(data.title);
            for (var i in data.subTasks) {
                var $el = $slide.find('.lesson__blocks').append(`
                    <div class="lesson__block">
                        <div class="lesson__subtitle">${data.subTasks[i].title}</div>
                        <div class="lesson__text">${data.subTasks[i].descriptions}</div>
                        <div class="lesson__shape" id="shape_${data.subTasks[i].id}"></div>
                    </div>
                `);
                init(`urn:${data.subTasks[i].urns}`, `shape_${data.subTasks[i].id}`);
            }
        });
    fetch(settings.remoteHost + '/rest/api/questions/next', settings.fetchOptions)
        .then(function (response) {return response.json();})
        .then(function (data) {
            var $slide = $('.lessons__item:eq(2)');
            $slide.find('.lesson__title').text(data.title);
            $slide.find('.lesson__question').text(data.question);
            for (var i in data.answers) {
                $slide.find('.lesson__answers').append(`<label><input type="radio" name="string" value="${data.answers[i]}">${data.answers[i]}</label>`)
            }
        });
    fetch(settings.remoteHost + '/rest/api/task/example/resolveTask', settings.fetchOptions)
        .then(function (response) {return response.json();})
        .then(function (data) {
            var $slide = $('.lessons__item:eq(3)');
            $slide.find('.lesson__title').text(data.title);
            $slide.find('.lesson__block').prepend(`<div class="lesson__task">
                                <div class="lesson__title">${data.title}</div>
                                <div class="lesson__text">${data.description}</div>
                                <div class="lesson__shape" id="shape_00"></div>
                            </div>`);
            init('urn:' + data.urn, "shape_00");
        });

    $('.js-make-file').click(function (e) {
        e.preventDefault();

        $('#file').html(
            '<select class="select"><option>Конус</option><option>Куб</option><option>Пирамида</option></select><br><br><input type="text" class="input" name="w" placeholder="Высота"><br><br><input type="text" class="input" name="h" placeholder="Основание">'
        );
    })

    $('.js-add-lesson').submit(function (e) {
        e.preventDefault();

        var data = $(this).serialize();

        console.log(data);

        var options = Object.assign(settings.fetchOptions);
        fetch(settings.remoteHost + '/rest/api/task/example/create/task?' + data, settings.fetchOptions)
            .then(function (response) {return response.json();})
            .then(function (data) {
                $('#nld').html(`<div class="lesson">
                                <div class="lesson__title">${data.title}</div>
                                <div class="lesson__text">${data.description}</div>
                                <div class="lesson__shape" id="shape_99"></div>
                            </div>`);
                init('urn:' + data.urn, "shape_99");
                router.navigate('/new_lesson');
            });
    })
});