package com.hackrussia.openknowledge.controllers.api;

import com.hackrussia.openknowledge.controllers.constants.ControllerConstants;
import com.hackrussia.openknowledge.mongodb.enitites.Question;
import com.hackrussia.openknowledge.mongodb.repos.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(ControllerConstants.API_PREFIX + "/questions")
public class QuestionController {

    @Autowired
    private QuestionRepository questionRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Question> getAllQuestions() {
        return questionRepository.findAll();
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    public ResponseEntity<Question> getNextQuestion() {
		MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
		headers.add("Access-Control-Allow-Origin","*");
		headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");

		ResponseEntity response = new ResponseEntity<>(questionRepository.findAll().get(0), headers, HttpStatus.OK);

		return response;
    }

	@RequestMapping(value = "/next/result", method = RequestMethod.GET)
	public ResponseEntity<Boolean> getCorrectResult(@RequestParam String answer) {
		MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
		headers.add("Access-Control-Allow-Origin","*");
		headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");

		Boolean result;
		if (answer.equals(questionRepository.findAll().get(0).getRightAnswer())) {
			result = true;
		} else {
			result = false;
		}

		ResponseEntity response = new ResponseEntity<>(result, headers, HttpStatus.OK);

		return response;
	}

}
