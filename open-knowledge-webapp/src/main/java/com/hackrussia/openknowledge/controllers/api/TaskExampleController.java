package com.hackrussia.openknowledge.controllers.api;


import com.hackrussia.openknowledge.controllers.constants.ControllerConstants;
import com.hackrussia.openknowledge.mongodb.enitites.TaskExample;
import com.hackrussia.openknowledge.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ControllerConstants.API_PREFIX + "/task")
public class TaskExampleController {

	@Autowired
	TaskService taskService;

	@RequestMapping(value = "/example/next", method = RequestMethod.GET)
	public ResponseEntity<TaskExample> getTaskExample() {

		TaskExample examples = taskService.getAllTaskExample().get(0);

		MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
		headers.add("Access-Control-Allow-Origin","*");
		headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");
		ResponseEntity response = new ResponseEntity(examples, headers, HttpStatus.OK);

		return response;
	}

	@RequestMapping(value = "/example/resolveTask", method = RequestMethod.GET)
	public ResponseEntity<TaskExample> getTaskResolve() {

		TaskExample taskExample = new TaskExample(2, "Нахождение высоты пирамиды");
		//задача с прямоугольной пирамидой
		taskExample.setUrn("dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDE2LTExLTIwLTAwLTE4LTU4LXVqZmZpdnoxMG45b2Znb3VnZHFidHhqd2VnYXQvMTkubWF4");
		taskExample.setRightAnswer("8");
		taskExample.setDescription("Дана квадратная пирамида со сторонами <br>" +
				"FD = 16 DC = 8&#8730;2 <br> " +
				"Задача: <br>" +
				"Найти высоту пирамиды FH. "); // 8 корней из 2

		MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
		headers.add("Access-Control-Allow-Origin","*");
		headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");
		ResponseEntity response = new ResponseEntity(taskExample, headers, HttpStatus.OK);

		return response;
	}

	@RequestMapping(value = "/example/resolveTask/check", method = RequestMethod.GET)
	public ResponseEntity<Boolean> resolveTask(@RequestParam String answer) {

		Boolean result;
		if (answer.equals(taskService.getAllTaskExample().get(0).getRightAnswer())) {
			result = true;
		} else {
			result = false;
		}

		MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
		headers.add("Access-Control-Allow-Origin","*");
		headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");
		ResponseEntity response = new ResponseEntity(result, headers, HttpStatus.OK);

		return response;
	}

	@RequestMapping(value = "/example/create/task", method = RequestMethod.GET)
	public ResponseEntity<TaskExample> getNewTask(@RequestParam  String h, @RequestParam String w, @RequestParam String title, @RequestParam String text) {

		String urn;
		if (h.equals("3")) {
			urn = "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDE2LTExLTIwLTAzLTQ3LTM4LWJldnN5dm56cWM2ZWdzdm9jaHV1ZXZzdDkzNW8vMTQubWF4";
		} else {
			urn = "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDE2LTExLTIwLTAzLTQ4LTI2LWJldnN5dm56cWM2ZWdzdm9jaHV1ZXZzdDkzNW8vMTUubWF4";
		}


		TaskExample taskExample = new TaskExample(3, title);
		taskExample.setDescription(text);
		taskExample.setUrn(urn);
		MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
		headers.add("Access-Control-Allow-Origin","*");
		headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");
		ResponseEntity response = new ResponseEntity(taskExample, headers, HttpStatus.OK);

		return response;
	}

}
