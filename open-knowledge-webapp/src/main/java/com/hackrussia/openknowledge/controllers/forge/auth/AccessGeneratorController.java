package com.hackrussia.openknowledge.controllers.forge.auth;

import com.hackrussia.openknowledge.controllers.constants.ControllerConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author amylnikov
 */
@Controller
@RequestMapping(value = ControllerConstants.API_PREFIX+"/forge/auth")
public class AccessGeneratorController {

    String token = null;

    @RequestMapping(value = {"/authToken"}, method = RequestMethod.GET)
    public ResponseEntity<String> getAuthToken() {

        String key = "574LLTyFAS3arY0kDAVKMY3ZXAYFhCIe";
        String secret = "6aRb7hmWeWxx7qvF";

        DataOutputStream output = null;
        InputStream input = null;
        BufferedReader buffer = null;

        if (token == null) {
            //get access token
            try {
                URL authenticationURL = new URL(
                        "https://developer.api.autodesk.com/authentication/v1/authenticate");
                HttpsURLConnection connection = (HttpsURLConnection) authenticationURL
                        .openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                connection.setDoOutput(true);
                connection.setDoInput(true);


                output = new DataOutputStream(connection.getOutputStream());
                output.writeBytes("client_id="
                        + URLEncoder.encode(key, "UTF-8")
                        + "&client_secret="
                        + URLEncoder.encode(secret, "UTF-8")
                        + "&grant_type=client_credentials"
                        + "&scope=data:read%20data:write%20bucket:create%20bucket:read");


                //get the response
                input = connection.getInputStream();
                buffer = new BufferedReader(new InputStreamReader(input));
                String line;
                StringBuffer stringBuffer = new StringBuffer();

                while ((line = buffer.readLine()) != null) {
                    stringBuffer.append(line);
                    stringBuffer.append('\r');
                }

                System.out.println(stringBuffer);

                //parse the response
                String responseString = stringBuffer.toString();
                int index = responseString.indexOf("\"access_token\":\"")
                        + "\"access_token\":\"".length();
                int index2 = responseString.indexOf("\"", index);
                token = responseString.substring(index, index2);

            } catch (IOException e) {
                System.out.println(e);
            }
        }

        MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
        headers.add("Access-Control-Allow-Origin","*");
        headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");

        ResponseEntity<String> response = new ResponseEntity<String>(token, headers, HttpStatus.OK);
        return response;
    }

}
