package com.hackrussia.openknowledge.controllers.forge.bucket;

import com.hackrussia.openknowledge.controllers.constants.ControllerConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;

/**
 * @author amylnikov
 */
@Controller
@RequestMapping(value = ControllerConstants.API_PREFIX + "/forge/bucket")
public class BucketController {

    private String bucketName = "edubucketname";

    @RequestMapping(value = {"/bucketName"}, method = RequestMethod.POST)
    public ResponseEntity<String> createBucket() {

        DataOutputStream output = null;
        InputStream input = null;
        BufferedReader buffer = null;

        try {

            URL bucketURL = new URL(
                    "https://developer.api.autodesk.com/oss/v1/buckets");
            HttpsURLConnection connection = (HttpsURLConnection) bucketURL
                    .openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + "H6YjltnmG1DXYHkE2keaXCfejhHO");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            output = new DataOutputStream(connection.getOutputStream());
            output.writeBytes("{\"bucketKey\":\"" + bucketName
                    + "\",\"policy\":\"transient\"}");

            // parse the response
            if (connection.getResponseCode() >= 400) {
                input = connection.getErrorStream();
            } else {
                input = connection.getInputStream();
            }
            buffer = new BufferedReader(new InputStreamReader(input));

            String line;
            StringBuffer stringBuffer = new StringBuffer();
            while ((line = buffer.readLine()) != null) {
                stringBuffer.append(line);
                stringBuffer.append('\r');
            }

            System.out.println(stringBuffer);

        } catch (IOException e) {
            System.out.println("Network connection error");
        }

        MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
        headers.add("Access-Control-Allow-Origin","*");
        headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");

        ResponseEntity<String> response = new ResponseEntity<String>(bucketName, headers, HttpStatus.OK);
        return response;
    }
}
