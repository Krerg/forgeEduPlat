package com.hackrussia.openknowledge.controllers.api;

import com.hackrussia.openknowledge.controllers.constants.ControllerConstants;
import com.hackrussia.openknowledge.mongodb.enitites.Role;
import com.hackrussia.openknowledge.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping(value = ControllerConstants.API_PREFIX + "/roles")
public class RoleController {

    @Autowired
    private AccountService accountService;
    //------------------- Get all Roles  --------------------------------------------------------

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Role>> getAllRoles() {

        List<Role> roles = accountService.findAllRoles();
        if(roles.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity(roles, HttpStatus.OK);
    }
}
