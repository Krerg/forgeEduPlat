package com.hackrussia.openknowledge.controllers.api;

import com.hackrussia.openknowledge.controllers.constants.ControllerConstants;
import com.hackrussia.openknowledge.mongodb.enitites.Role;
import com.hackrussia.openknowledge.mongodb.enitites.User;
import com.hackrussia.openknowledge.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(ControllerConstants.API_PREFIX + "/users")
public class AccountController {

	private static Logger log = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    AccountService accountService;
//
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<User>> listAllUsers() {

        List<User> users = new ArrayList<>();

        if (users.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") long id) {

        log.debug("Fetching User with id " + id);

        User user = accountService.findUserById(id);
        log.debug("Found user: ", user);

        return new ResponseEntity(user, HttpStatus.OK);
    }


    //-------------------Create a User--------------------------------------------------------

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createUser(@Valid @RequestBody User user, UriComponentsBuilder ucBuilder) throws Exception {

        log.debug("Creating User " + user.getUsername());
        accountService.saveUser(user);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/users/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity(headers, HttpStatus.CREATED);

    }


    //------------------- Update a User --------------------------------------------------------

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@Valid @PathVariable("id") long id, @RequestBody User user) {
        log.debug("Updating User " + id);

        user.setId(id);

        accountService.updateUser(user);
        log.debug("Updated user: ", user);

        User updatedUser = accountService.findUserById(user.getId());
        return new ResponseEntity(updatedUser, HttpStatus.OK);
    }

    //------------------- Delete User --------------------------------------------------------

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") long id) {
        log.debug("Fetching & Deleting User with id " + id);

        User user = accountService.findUserById(id);

        accountService.deleteUserById(id);
        log.debug("Deleted user: ", user);

        return new ResponseEntity(HttpStatus.NO_CONTENT); // Выполнено успешно, но нет контента для показа
    }

    //------------------- Get Roles from User --------------------------------------------------------
    @RequestMapping(value = "/{id}/roles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Role>> getAllRoles(@PathVariable("id") long id) {

        User user = accountService.findUserById(id);

        List<Role> roles = new ArrayList(user.getAuthorities());
        if(roles.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity(roles, HttpStatus.OK);
    }

}
