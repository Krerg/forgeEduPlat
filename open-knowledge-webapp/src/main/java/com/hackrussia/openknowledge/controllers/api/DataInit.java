package com.hackrussia.openknowledge.controllers.api;

import com.hackrussia.openknowledge.controllers.constants.ControllerConstants;
import com.hackrussia.openknowledge.mongodb.enitites.Question;
import com.hackrussia.openknowledge.mongodb.enitites.SubTask;
import com.hackrussia.openknowledge.mongodb.enitites.TaskExample;
import com.hackrussia.openknowledge.mongodb.repos.QuestionRepository;
import com.hackrussia.openknowledge.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(ControllerConstants.API_PREFIX + "/data/init")
public class DataInit {

	@PostConstruct
	public void clearTaskData() {
		taskService.clearAll();
	}

	@Autowired
	TaskService taskService;

	@Autowired
	QuestionRepository questionRepository;

	@RequestMapping(method = RequestMethod.GET)
	public void dataInit() {
		String urn1 = "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDE2LTExLTIwLTAwLTE4LTUzLXVqZmZpdnoxMG45b2Znb3VnZHFidHhqd2VnYXQvMTEwLm1heA";
		String urn21 = "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDE2LTExLTIwLTAwLTE4LTQ5LXVqZmZpdnoxMG45b2Znb3VnZHFidHhqd2VnYXQvMTExLm1heA";
		String urn22 = "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDE2LTExLTIwLTAwLTE4LTQ0LXVqZmZpdnoxMG45b2Znb3VnZHFidHhqd2VnYXQvMTEyLm1heA";
		String urn3 = "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDE2LTExLTIwLTA1LTE1LTM1LWJldnN5dm56cWM2ZWdzdm9jaHV1ZXZzdDkzNW8vNC5tYXg";

				SubTask axiom1 = new SubTask(1, "Аксиома 1.", urn1, "Через любые три точки, не лежащие на одной прямой, проходит плоскость, и притом только одна.");
		SubTask axiom21 = new SubTask(2, "Аксиома 2.", urn21, "Если две точки прямой лежат в плоскости, то все точки прямой лежат в этой плоскости. (Прямая лежит на плоскости или плоскость проходит через прямую).");
		SubTask axiom22 = new SubTask(3, "", urn22, "Из аксиомы 2 следует, что если прямая не лежит в данной плоскости, то она имеет с ней не более одной общей точки. Если прямая и плоскость имеют одну общую точку, то говорят, что они пересекаются.");
		SubTask axiom3 = new SubTask(4, "Аксиома 3.", urn3, "Если две различные плоскости имеют общую точку, то они имеют общую прямую, на которой лежат все общие точки этих плоскостей.<br>" +
				"<br>" +
				"В таком случае говорят, плоскости пересекаются по прямой.\n" +
				"<br>" +
				"Пример: пересечение двух смежных стен, стены и потолка комнаты.");
		List<SubTask> subTasks = new ArrayList<>();
		subTasks.add(axiom1);
		subTasks.add(axiom21);
		subTasks.add(axiom22);
		subTasks.add(axiom3);
		TaskExample axioms = new TaskExample(1, "Аксиомы стереометрии:");
		axioms.setSubTasks(subTasks);

		taskService.addTaskExample(axioms);

		List<String> answersOnQuestion = new ArrayList<>();
		String question = "Дополните предложение: \n Если две точки лежат в плоскости то ...";
		String a1 = "все точки прямой образают куб";
		String a2 = "все точки прямой лежат в этой плоскости";
		String a3 = "все точки прямой не лежат в одной плоскости";
		String a4 = "ни одна точка не лежит в этой плоскости";
		answersOnQuestion.add(a1);
		answersOnQuestion.add(a2);
		answersOnQuestion.add(a3);
		answersOnQuestion.add(a4);
		String rightAnswer = "все точки прямой лежат в это плоскости";
		String title = "Проверка знания базовых аксиом стереометрии";
		Question questionAboutAxiom = new Question(question, answersOnQuestion, rightAnswer, title);
		questionRepository.save(questionAboutAxiom);
	}
}
