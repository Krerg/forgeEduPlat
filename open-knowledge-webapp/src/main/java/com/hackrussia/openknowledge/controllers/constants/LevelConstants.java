package com.hackrussia.openknowledge.controllers.constants;

import java.util.Map;

/**
 * @author amylnikov
 */
public class LevelConstants {

    private static Map<Integer, Integer> levelExperience;

    static {
        levelExperience.put(0,100);
        levelExperience.put(1,300);
        levelExperience.put(2,550);
        levelExperience.put(3,800);
    }

    public static int getLevelExperience(int level) {
        return levelExperience.get(level);
    }

}
