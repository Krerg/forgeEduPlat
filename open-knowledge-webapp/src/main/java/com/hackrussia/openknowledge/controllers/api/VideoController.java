package com.hackrussia.openknowledge.controllers.api;


import com.hackrussia.openknowledge.controllers.constants.ControllerConstants;
import com.hackrussia.openknowledge.mongodb.enitites.Video;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ControllerConstants.API_PREFIX + "/video")
public class VideoController {

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	public ResponseEntity<Video> getNextVideo() {
		MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
		headers.add("Access-Control-Allow-Origin","*");
		headers.add("Access-Control-Allow-Methods","GET, POST, OPTIONS");

		ResponseEntity response = new ResponseEntity<>(new Video("https://www.youtube.com/embed/DahThUey2rk","Аксиомы стереометрии"), headers, HttpStatus.OK);
		return response;
	}

}
