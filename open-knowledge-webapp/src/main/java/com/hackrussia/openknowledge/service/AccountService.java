package com.hackrussia.openknowledge.service;

import com.hackrussia.openknowledge.mongodb.enitites.Role;
import com.hackrussia.openknowledge.mongodb.enitites.TaskStudent;
import com.hackrussia.openknowledge.mongodb.enitites.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface AccountService extends UserDetailsService {

    User findUserById(long id);
    User findUserByLogin(String login);
    void saveUser(User user) throws Exception;
    void updateUser(User user);
    void deleteUserById(long id);
    List<User> findAllUsers();

    Role findRoleByName(String groupName);
    Role findRoleById(Long id);
    List<Role> findAllRoles();
    void updateRole(Role role);
    void calculateExperience(User user, TaskStudent task);
}
