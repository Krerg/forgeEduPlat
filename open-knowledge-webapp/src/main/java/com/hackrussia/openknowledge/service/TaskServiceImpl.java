package com.hackrussia.openknowledge.service;

import com.hackrussia.openknowledge.mongodb.enitites.Question;
import com.hackrussia.openknowledge.mongodb.enitites.SubTask;
import com.hackrussia.openknowledge.mongodb.enitites.TaskExample;
import com.hackrussia.openknowledge.mongodb.enitites.TaskStudent;
import com.hackrussia.openknowledge.mongodb.repos.QuestionRepository;
import com.hackrussia.openknowledge.mongodb.repos.SubTaskRepository;
import com.hackrussia.openknowledge.mongodb.repos.TaskExampleRepository;
import com.hackrussia.openknowledge.mongodb.repos.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	TaskRepository taskRepository;

	@Autowired
	SubTaskRepository subTaskRepository;

	@Autowired
	TaskExampleRepository taskExampleRepository;

	@Autowired
	QuestionRepository questionRepository;

	@Override
	public TaskStudent getTaskById(Long id) {
		return taskRepository.findOneByField("id",id);
	}

	@Override
	public TaskStudent getNextTask() {
		return taskRepository.findAll().get(0);
	}

	@Override
	public boolean addTask(TaskStudent task) {
		try {
			taskRepository.save(task);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public TaskExample getTaskExampleById(Long id) {
		return taskExampleRepository.findOneByField("id",id);
	}

	@Override
	public TaskExample getNextTaskExample() {
		return taskExampleRepository.findAll().get(0);
	}

	@Override
	public List<TaskExample> getAllTaskExample() {
		return taskExampleRepository.findAll();
	}

	@Override
	public boolean addTaskExample(TaskExample taskExample) {
		try {
			taskExampleRepository.save(taskExample);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public SubTask getTaskSubExampleById(Long id) {
		return subTaskRepository.findOneByField("id",id);
	}

	@Override
	public SubTask getNextSubTaskExample() {
		return subTaskRepository.findAll().get(0);
	}

	@Override
	public List<SubTask> getAllSubTaskExample() {
		return subTaskRepository.findAll();
	}

	@Override
	public boolean addSubTaskExample(SubTask subTask) {
		try {
			subTaskRepository.save(subTask);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void clearAll() {
		for(TaskStudent taskExample : taskRepository.findAll()) {
			taskRepository.delete(taskExample);
		}
		for(TaskExample taskExample : taskExampleRepository.findAll()) {
			taskExampleRepository.delete(taskExample);
		}
		for(Question question : questionRepository.findAll()) {
			questionRepository.delete(question);
		}
	}

}
