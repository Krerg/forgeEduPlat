package com.hackrussia.openknowledge.service;


import com.hackrussia.openknowledge.controllers.constants.LevelConstants;
import com.hackrussia.openknowledge.mongodb.enitites.Role;
import com.hackrussia.openknowledge.mongodb.enitites.TaskStudent;
import com.hackrussia.openknowledge.mongodb.enitites.User;
import com.hackrussia.openknowledge.mongodb.repos.RoleRepository;
import com.hackrussia.openknowledge.mongodb.repos.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;


@Service
public class AccountServiceImpl implements AccountService, UserDetailsService {

	private static final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);

	public AccountServiceImpl() {
	}

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public User findUserById(long id) {
		return userRepository.findAll().get(0); // or getOne()????
	}

	@Override
	public User findUserByLogin(String login) {
		return userRepository.findByLogin(login);
	}

	@Override
	public void saveUser(User user) throws Exception {
		setNewAuthoritiesToUser(user, user.getAuthorities());
		userRepository.save(user);
		log.debug("Saved user: ", user);
	}


	// присваивае троли пользователю только в том случае, если они уже существуют в системе, иначе выбрасывае тисключение AuthoritiesNotExist
	private void setNewAuthoritiesToUser(User user, Collection<? extends GrantedAuthority> authorities) {

		// Достаем роли из бд и сравниваем их с ролями, пришедшими с новым юзером
		// Если разность двух множест ролей не пуста, значит, пользователю пытаются присвоить неправильные роли ( несуществующие в системе )
		HashSet<Role> roles = (HashSet<Role>) authorities;
		HashSet<Role> tempRoles = new HashSet(findAllRoles());

		// Если роли не переданы (null), ставим по умолчанию роль ROLE_USER
		if (roles == null || roles.isEmpty()) {
			roles = new HashSet<>();
			roles.add(new Role("ROLE_USER"));
		}

		// разность множеств
		HashSet<Role> diff = (HashSet<Role>) roles.clone();
		diff.removeAll(tempRoles);


		HashSet<Role> rolesToBeAdded = new HashSet<>();

		// извлекаем роли (вместе с их id)
		for (Role role : roles) {
			rolesToBeAdded.add(findRoleByName(role.getName()));
		}
		// присваиваем их пользователю
		user.setAuthorities(rolesToBeAdded);
	}

	@Override
	public void updateUser(User user) {
		User userInDatabase = userRepository.findOneByField("id", user.getId());

		if (user.getUsername() != null) {
			userInDatabase.setUsername(user.getUsername());
		}
		if (user.getPassword() != null)
			userInDatabase.setPassword(user.getPassword());
		if (user.isNonLocked() != userInDatabase.isNonLocked())
			userInDatabase.setNonLocked(!userInDatabase.isNonLocked());
		if (user.isEnabled() != userInDatabase.isEnabled())
			userInDatabase.setEnabled(!userInDatabase.isEnabled());
		if (user.getAuthorities() != null) {
			setNewAuthoritiesToUser(userInDatabase, user.getAuthorities());
		}

		userRepository.save(userInDatabase);
	}

	@Override
	public void deleteUserById(long id) {
		userRepository.delete(userRepository.findOneByField("id",id));
	}

	@Override
	public List<User> findAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public Role findRoleByName(String groupName) {
		return roleRepository.findOneByField("name", groupName);
	}

	@Override
	public Role findRoleById(Long id) {
		return roleRepository.findOneByField("id",id);
	}

	@Override
	public List<Role> findAllRoles() {
		return roleRepository.findAll();
	}

	@Override
	public void updateRole(Role role) {
		roleRepository.save(role);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User person = userRepository.findByLogin(username);

		if (person == null)
			throw new UsernameNotFoundException("Username " + username + " not found");

		return person;
	}


	@Override
	public void calculateExperience(User user, TaskStudent task) {
		int experienceForUserLevel = LevelConstants.getLevelExperience(user.getLevel());
		int summedExperience = user.getExperience() + task.getExp();
		if (summedExperience > experienceForUserLevel) {
			user.setLevel(user.getLevel() + 1);
			user.setExperience(summedExperience - experienceForUserLevel);
		} else {
			user.setExperience(summedExperience);
		}
	}
}
