package com.hackrussia.openknowledge.service;

import com.hackrussia.openknowledge.mongodb.enitites.SubTask;
import com.hackrussia.openknowledge.mongodb.enitites.TaskExample;
import com.hackrussia.openknowledge.mongodb.enitites.TaskStudent;

import java.util.List;

public interface TaskService {
	TaskStudent getTaskById(Long id);
	TaskStudent getNextTask();
	boolean addTask(TaskStudent task);

	TaskExample getTaskExampleById(Long id);
	TaskExample getNextTaskExample();
	List<TaskExample> getAllTaskExample();
	boolean addTaskExample(TaskExample taskExample);


	/**
	 * Drops task collection.
	 */
	void clearAll();

	SubTask getTaskSubExampleById(Long id);
	SubTask getNextSubTaskExample();
	List<SubTask> getAllSubTaskExample();
	boolean addSubTaskExample(SubTask subTask);

}
