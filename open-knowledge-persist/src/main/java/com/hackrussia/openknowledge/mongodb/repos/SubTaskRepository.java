package com.hackrussia.openknowledge.mongodb.repos;

import com.hackrussia.openknowledge.mongodb.enitites.SubTask;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

@Repository
public class SubTaskRepository extends MongoRepository<SubTask, ObjectId> {
	public SubTaskRepository() {
		super(SubTask.class);
	}
}
