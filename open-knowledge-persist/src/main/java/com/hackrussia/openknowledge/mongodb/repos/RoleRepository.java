package com.hackrussia.openknowledge.mongodb.repos;

import com.hackrussia.openknowledge.mongodb.enitites.Role;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepository extends MongoRepository<Role, ObjectId> {

    public RoleRepository() {
        super(Role.class);
    }

//    @Query("select role from Role role where role.name = :roleName")
//    Role findByName(@Param("roleName") String roleName);
//
//    @Query("SELECT CASE WHEN COUNT(role) > 0 THEN 'true' ELSE 'false' END FROM Role role where role.name = ?1")
//    Boolean existsByName(@Param("roleName") String roleName);
}
