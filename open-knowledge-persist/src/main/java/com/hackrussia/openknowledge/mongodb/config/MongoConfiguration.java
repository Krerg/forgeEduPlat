package com.hackrussia.openknowledge.mongodb.config;

import com.hackrussia.openknowledge.mongodb.enitites.*;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.UnknownHostException;

@Configuration
public class MongoConfiguration {

    private static final String MONGO_HOST_NAME = "localhost";

    private static final int MONGO_PORT = 27017;

    public static final String MONGO_DATABSE_NAME = "fep";

    @Bean
    public Datastore getDB() throws UnknownHostException {
        Mongo mongo = new MongoClient(new ServerAddress(MONGO_HOST_NAME, MONGO_PORT));
        Morphia morphia = new Morphia();
        morphia.map(User.class);
        morphia.map(Role.class);
        morphia.map(TaskStudent.class);
        morphia.map(TaskExample.class);
        morphia.map(TitleTable.class);
        morphia.map(Video.class);
        return morphia.createDatastore((MongoClient) mongo, MONGO_DATABSE_NAME);
    }

}
