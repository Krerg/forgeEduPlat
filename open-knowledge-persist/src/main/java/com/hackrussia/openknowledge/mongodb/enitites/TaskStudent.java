package com.hackrussia.openknowledge.mongodb.enitites;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity
public class TaskStudent {
	private static final long serialVersionUID = -4727727495060874309L;

	@Id
	private long id;
	private String title;
	private String urn;
	private String description;
	private int exp;
	private String answer;
	private String rightAnswer;

	public TaskStudent() {

	}

	public TaskStudent(String title, String urn, String description, int exp, String rightAnswer) {
		this.title = title;
		this.urn = urn;
		this.description = description;
		this.exp = exp;
		this.rightAnswer = rightAnswer;
	}

	public String getUrn() {
		return urn;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getRightAnswer() {
		return rightAnswer;
	}

	public void setRightAnswer(String rightAnswer) {
		this.rightAnswer = rightAnswer;
	}
}
