package com.hackrussia.openknowledge.mongodb.enitites;

import org.mongodb.morphia.annotations.Id;

public class SubTask {

	@Id
	private long id;
	private String title;
	private String urns;
	private String descriptions;

	public SubTask() {

	}

	public SubTask(long id, String title, String urns, String descriptions) {
		this.id = id;
		this.title = title;
		this.urns = urns;
		this.descriptions = descriptions;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrns() {
		return urns;
	}

	public void setUrns(String urns) {
		this.urns = urns;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}
}
