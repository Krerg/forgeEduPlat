package com.hackrussia.openknowledge.mongodb.enitites;

import org.mongodb.morphia.annotations.Id;

public class Video {
	private static final long serialVersionUID = -4727727495060874309L;

	@Id
	private long id;
	private String url;
	private String title;

	public Video(String url, String title) {
		this.url = url;
		this.title = title;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
