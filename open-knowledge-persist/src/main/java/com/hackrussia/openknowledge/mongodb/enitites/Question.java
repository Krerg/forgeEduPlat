package com.hackrussia.openknowledge.mongodb.enitites;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

@Entity
public class Question {

	private static final long serialVersionUID = -4727727495060874309L;

	@Id
	private ObjectId id;
	private String question;
	private List<String> answers;
	private String rightAnswer;
	private String title;

	public Question() {

	}

	public Question(String question, List<String> answers, String rightAnswer, String title) {
		this.question = question;
		this.answers = answers;
		this.rightAnswer = rightAnswer;
		this.title = title;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	public String getRightAnswer() {
		return rightAnswer;
	}

	public void setRightAnswer(String rightAnswer) {
		this.rightAnswer = rightAnswer;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
