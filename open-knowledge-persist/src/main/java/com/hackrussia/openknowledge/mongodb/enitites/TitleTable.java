package com.hackrussia.openknowledge.mongodb.enitites;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

@Entity
public class TitleTable { // оглавление
	private static final long serialVersionUID = -4727727495060874309L;
	@Id
	private long id;
	private String mainTitles; // название урока
	private List<String> titles; // список тем урока

	public TitleTable() {

	}

	public TitleTable(String mainTitles, List<String> titles) {
		this.mainTitles = mainTitles;
		this.titles = titles;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMainTitles() {
		return mainTitles;
	}

	public void setMainTitles(String mainTitles) {
		this.mainTitles = mainTitles;
	}

	public List<String> getTitles() {
		return titles;
	}

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}
}
