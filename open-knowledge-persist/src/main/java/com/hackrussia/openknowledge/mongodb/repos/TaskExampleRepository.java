package com.hackrussia.openknowledge.mongodb.repos;

import com.hackrussia.openknowledge.mongodb.enitites.TaskExample;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

@Repository
public class TaskExampleRepository extends MongoRepository<TaskExample, ObjectId> {
	public TaskExampleRepository() {
		super(TaskExample.class);
	}

}
