package com.hackrussia.openknowledge.mongodb.repos;

import com.hackrussia.openknowledge.mongodb.enitites.Question;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

@Repository
public class QuestionRepository extends MongoRepository<Question,ObjectId> {

    /**
     * Public constructor. Need to be passed entity class for default methods implemetation.
     *
     */
    public QuestionRepository() {
        super(Question.class);
    }
}
