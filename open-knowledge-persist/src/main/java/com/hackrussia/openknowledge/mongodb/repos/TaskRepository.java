package com.hackrussia.openknowledge.mongodb.repos;

import com.hackrussia.openknowledge.mongodb.enitites.TaskStudent;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

@Repository
public class TaskRepository extends MongoRepository<TaskStudent, ObjectId> {

	public TaskRepository() {
		super(TaskStudent.class);
	}
}
